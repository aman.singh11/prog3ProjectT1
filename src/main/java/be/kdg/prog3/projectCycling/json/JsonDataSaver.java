package be.kdg.prog3.projectCycling.json;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

@Component
public class JsonDataSaver {

    public static void saveDataToJson(String fileName, List<?> data) {
        try {
            final ByteArrayOutputStream out = new ByteArrayOutputStream();
            final ObjectMapper mapper = new ObjectMapper();
            mapper.writerWithDefaultPrettyPrinter().writeValue(out, data);
            Files.write(Path.of(fileName), out.toByteArray());
            System.out.println("Data saved to " + fileName + " at the project root folder...");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Failed to save data to " + fileName + " at the project root folder...");
        }
    }

}