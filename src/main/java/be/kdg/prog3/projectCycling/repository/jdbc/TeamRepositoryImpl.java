package be.kdg.prog3.projectCycling.repository.jdbc;

import be.kdg.prog3.projectCycling.domain.Cyclist;
import be.kdg.prog3.projectCycling.domain.Team;
import be.kdg.prog3.projectCycling.repository.TeamRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Profile("jdbc")
public class TeamRepositoryImpl implements TeamRepository {

    private final JdbcTemplate jdbcTemplate;
    private final SimpleJdbcInsert teamInserter;

    @Autowired
    public TeamRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.teamInserter = new SimpleJdbcInsert(jdbcTemplate)
                .withTableName("TEAMS")
                .usingGeneratedKeyColumns("ID");
    }

    public static Team mapTeamRow(ResultSet rs, int rowid) throws SQLException {
        return new Team(rs.getInt("ID"),
                rs.getString("TEAMNAME"),
                rs.getString("SPONSOR"),
                rs.getString("COUNTRY"),
                rs.getString("BIKEBRAND"),
                rs.getInt("FOUNDED"));
    }


    public static Team mapTeamRowWithCyclist(ResultSet rs, int rowid) throws SQLException {
        Team team = new Team(
                rs.getInt("ID"),
                rs.getString("TEAMNAME"),
                rs.getString("SPONSOR"),
                rs.getString("COUNTRY"),
                rs.getString("BIKEBRAND"),
                rs.getInt("FOUNDED"));

        // Check if there are associated cyclists
        int cyclistId = rs.getInt("ID");
        if (cyclistId != 0) {
            Cyclist cyclist = new Cyclist(
                    cyclistId,
                    rs.getString("NAME"),
                    rs.getInt("AGE"),
                    rs.getString("NATIONALITY"),
                    rs.getDouble("HEIGHT"),
                    rs.getInt("TEAM_ID"));
            // Add the cyclist to the team
            team.addCyclist(cyclist);
        }

        return team;
    }


    @Override
    public List<Team> findAll() {
        return jdbcTemplate.query("SELECT * FROM TEAMS", TeamRepositoryImpl::mapTeamRow);
    }

    @Override
    public List<Team> findAllWithCyclist() {
        return jdbcTemplate.query("SELECT T.*, C.*, C.ID AS ID_Cyclist\n" +
                "FROM CYCLISTS C\n" +
                "JOIN TEAMS T ON T.ID = C.TEAM_ID", TeamRepositoryImpl::mapTeamRowWithCyclist);
    }


    @Override
    public Team findById(int id) {
        return jdbcTemplate.queryForObject("SELECT * FROM TEAMS WHERE ID = ?", TeamRepositoryImpl::mapTeamRow, id);
    }

    @Override
    public void createTeam(Team team) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("ID", team.getId());
        parameters.put("TEAMNAME", team.getTeamName());
        parameters.put("SPONSOR", team.getSponsor());
        parameters.put("COUNTRY", team.getCountry());
        parameters.put("BIKEBRAND", team.getBikeBrand());
        parameters.put("FOUNDED", team.getFounded());
        team.setId(teamInserter.executeAndReturnKey(parameters).intValue());
    }

    @Override
    public void updateTeam(Team team) {
        Team existingTeam = findById(team.getId());

        // Update the fields
        existingTeam.setTeamName(team.getTeamName());
        existingTeam.setCountry(team.getCountry());
        existingTeam.setSponsor(team.getSponsor());
        existingTeam.setBikeBrand(team.getBikeBrand());
        existingTeam.setFounded(team.getFounded());


        jdbcTemplate.update("UPDATE TEAMS SET TEAMNAME=?, SPONSOR=?, COUNTRY=?, BIKEBRAND=?, FOUNDED=? WHERE ID=?",
                existingTeam.getTeamName(), existingTeam.getSponsor(), existingTeam.getCountry(), existingTeam.getBikeBrand(), existingTeam.getFounded(), existingTeam.getId());
    }

    @Override
    @Transactional
    public void deleteTeam(int id) {
        // Detach cyclists from the team
        jdbcTemplate.update("UPDATE CYCLISTS SET TEAM_ID=NULL WHERE TEAM_ID=?", id);

        // Delete the team
        jdbcTemplate.update("DELETE FROM TEAMS WHERE ID=?", id);
    }

}
