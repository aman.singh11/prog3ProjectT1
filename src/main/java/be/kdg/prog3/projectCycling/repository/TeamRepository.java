package be.kdg.prog3.projectCycling.repository;

import be.kdg.prog3.projectCycling.domain.Team;

import java.util.List;


public interface TeamRepository {

    List<Team> findAll();

    List<Team> findAllWithCyclist();

    Team findById(int id);

    void createTeam(Team team);

    void updateTeam(Team team);

    void deleteTeam(int id);
}
