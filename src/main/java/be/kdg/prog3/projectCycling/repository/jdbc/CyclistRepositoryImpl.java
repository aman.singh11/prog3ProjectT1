package be.kdg.prog3.projectCycling.repository.jdbc;

import be.kdg.prog3.projectCycling.domain.Cyclist;
import be.kdg.prog3.projectCycling.domain.Race;
import be.kdg.prog3.projectCycling.domain.Team;
import be.kdg.prog3.projectCycling.repository.CyclistRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Profile("jdbc")
public class CyclistRepositoryImpl implements CyclistRepository {
    private final JdbcTemplate jdbcTemplate;
    private final SimpleJdbcInsert cyclistInserter;

    @Autowired
    public CyclistRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.cyclistInserter = new SimpleJdbcInsert(jdbcTemplate)
                .withTableName("CYCLISTS")
                .usingGeneratedKeyColumns("ID");
    }

    public static Cyclist mapCyclistRow(ResultSet rs, int rowid) throws SQLException {
        return new Cyclist(rs.getInt("ID"),
                rs.getString("NAME"),
                rs.getInt("AGE"),
                rs.getString("NATIONALITY"),
                rs.getDouble("HEIGHT"),
                rs.getInt("TEAM_ID"));
    }

    public static Cyclist mapCyclistRowWithTeamAndRace(ResultSet rs, int rowid) throws SQLException {
        Cyclist cyclist = new Cyclist(
                rs.getInt("ID"),
                rs.getString("NAME"),
                rs.getInt("AGE"),
                rs.getString("NATIONALITY"),
                rs.getDouble("HEIGHT"),
                rs.getInt("TEAM_ID"));

        if (rs.getInt("TEAM_ID") != 0) {
            Team team = new Team(
                    rs.getInt("TEAM_ID"),
                    rs.getString("TEAMNAME"),
                    rs.getString("SPONSOR"),
                    rs.getString("COUNTRY"),
                    rs.getString("BIKEBRAND"),
                    rs.getInt("FOUNDED"));
            cyclist.setTeam(team);
            team.addCyclist(cyclist);
        }

        if (rs.getInt("ID") != 0) {
            Timestamp raceDateLocal = rs.getTimestamp("RACEDATE");

            LocalDate raceDate = null;
            if (raceDateLocal != null) {
                raceDate = raceDateLocal.toInstant()
                        .atZone(ZoneId.systemDefault())
                        .toLocalDate();
            }

            Race race = new Race(
                    rs.getInt("ID"),
                    rs.getString("RACENAME"),
                    raceDate,
                    rs.getString("LOCATION"),
                    rs.getString("CATEGORY"),
                    rs.getInt("CYCLIST_ID"));
            cyclist.addRace(race);
            race.addCyclist(cyclist);
        }

        return cyclist;
    }


    @Override
    public List<Cyclist> findAllWithTeamAndRace() {
        return jdbcTemplate.query("SELECT \n" +
                "    C.*, \n" +
                "    T.*, \n" +
                "    R.* \n" +
                "FROM CYCLISTS C \n" +
                "LEFT JOIN CYCLIST_RACE CR ON C.ID = CR.ID_Cyclist \n" +
                "LEFT JOIN RACES R ON CR.ID_Races = R.ID \n" +
                "LEFT JOIN TEAMS T ON C.TEAM_ID = T.ID;\n", CyclistRepositoryImpl::mapCyclistRowWithTeamAndRace);
    }


    @Override
    public List<Cyclist> findAll() {
        return jdbcTemplate.query("SELECT * FROM CYCLISTS", CyclistRepositoryImpl::mapCyclistRow);
    }


    @Override
    public Cyclist findById(int id) {
        try {
            return jdbcTemplate.queryForObject("SELECT * FROM CYCLISTS WHERE ID = ?", CyclistRepositoryImpl::mapCyclistRow, id);
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    @Override
    public void createCyclist(Cyclist cyclist) {
        Map<String, Object> cyclistParameters = new HashMap<>();
        cyclistParameters.put("NAME", cyclist.getName());
        cyclistParameters.put("AGE", cyclist.getAge());
        cyclistParameters.put("NATIONALITY", cyclist.getNationality());
        cyclistParameters.put("HEIGHT", cyclist.getHeight());

        // Execute and return the generated key
        cyclist.setId(cyclistInserter.executeAndReturnKey(cyclistParameters).intValue());
    }


    @Override
    public void updateCyclist(Cyclist cyclist) {
        // Check if the cyclist exists
        Cyclist existingCyclist = findById(cyclist.getId());

        if (existingCyclist == null) {
            throw new RuntimeException("Cyclist with ID " + cyclist.getId() + " not found.");
        }

        // Update the cyclist properties
        existingCyclist.setName(cyclist.getName());
        existingCyclist.setAge(cyclist.getAge());
        existingCyclist.setNationality(cyclist.getNationality());
        existingCyclist.setTeamId(cyclist.getTeamId());

        // Perform the update
        jdbcTemplate.update("UPDATE CYCLISTS SET NAME=?, AGE=?, NATIONALITY=?, TEAM_ID=? WHERE ID=?",
                existingCyclist.getName(), existingCyclist.getAge(),
                existingCyclist.getNationality(), existingCyclist.getTeamId(), existingCyclist.getId());
    }


    @Override
    @Transactional
    public void deleteCyclist(int id) {
        jdbcTemplate.update("UPDATE RACES SET CYCLIST_ID=NULL WHERE CYCLIST_ID=?", id);

        jdbcTemplate.update("DELETE FROM CYCLIST_RACE WHERE ID_CYCLIST=?", id);

        jdbcTemplate.update("DELETE FROM CYCLISTS WHERE ID=?", id);
    }


    @Override
    public List<Cyclist> getYoungCyclist() {
        return jdbcTemplate.query("SELECT * FROM CYCLISTS WHERE AGE <= 24", CyclistRepositoryImpl::mapCyclistRow);
    }

    @Override
    public List<Cyclist> getOldCyclist() {
        return jdbcTemplate.query("SELECT * FROM CYCLISTS WHERE AGE > 24", CyclistRepositoryImpl::mapCyclistRow);
    }


    @Override
    public List<Cyclist> getCyclistWithMostWins() {
        return jdbcTemplate.query("SELECT c.*, COUNT(r.id) AS win_count " +
                "FROM cyclists c " +
                "JOIN cyclist_race cr ON c.id = cr.id_cyclist " +
                "JOIN races r ON cr.id_races = r.id " +
                "GROUP BY c.id, c.name " +
                "ORDER BY win_count DESC, c.NAME " +
                "LIMIT 1 OFFSET 0", CyclistRepositoryImpl::mapCyclistRow);
    }


}


