package be.kdg.prog3.projectCycling.repository.jpa;

import be.kdg.prog3.projectCycling.domain.Cyclist;
import be.kdg.prog3.projectCycling.domain.Team;
import be.kdg.prog3.projectCycling.repository.TeamRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Profile("jpa")
public class JPATeamRepositoryImpl implements TeamRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Team> findAll() {
        return em.createQuery("select team from Team team", Team.class)
                .getResultList();
    }

    @Override
    public List<Team> findAllWithCyclist() {
        return em.createQuery("select team from Team team", Team.class)
                .getResultList();
    }

    @Override
    public Team findById(int id) {
        Team team = em.find(Team.class, id);
        return team;
    }

    @Override
    @Transactional
    public void createTeam(Team team) {
        em.persist(team);
    }

    @Override
    @Transactional
    public void updateTeam(Team team) {
        em.merge(team);
    }

    @Override
    @Transactional
    public void deleteTeam(int id) {
        Team team = em.find(Team.class, id);

        if (team != null) {
            for (Cyclist cyclist : team.getCyclists()) {
                cyclist.setTeamId(null);
                cyclist.setTeam(null);
            }

            em.remove(team);
        }
    }

}
