package be.kdg.prog3.projectCycling.repository.jpa;

import be.kdg.prog3.projectCycling.domain.Cyclist;
import be.kdg.prog3.projectCycling.domain.Race;
import be.kdg.prog3.projectCycling.repository.CyclistRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Profile("jpa")
public class JPACyclistRepositoryImpl implements CyclistRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Cyclist> findAll() {
        return em.createQuery("select cyclist from Cyclist cyclist", Cyclist.class)
                .getResultList();
    }

    @Override
    public List<Cyclist> findAllWithTeamAndRace() {
        return em.createQuery("select cyclist from Cyclist cyclist", Cyclist.class)
                .getResultList();
    }


    @Override
    public Cyclist findById(int id) {
        Cyclist cyclist = em.find(Cyclist.class, id);
        return cyclist;

    }

    @Override
    @Transactional
    public void createCyclist(Cyclist cyclist) {
        em.persist(cyclist);
    }

    @Override
    @Transactional
    public void updateCyclist(Cyclist cyclist) {
        em.merge(cyclist);
    }

    @Override
    @Transactional
    public void deleteCyclist(int id) {
        Cyclist cyclist = em.find(Cyclist.class, id);

        if (cyclist != null) {
            for (Race race : cyclist.getRaces()) {
                race.setCyclistID(null);
                race.setCyclists(null);
            }

            em.remove(cyclist);
        }
    }




    @Override
    public List<Cyclist> getYoungCyclist() {
        return em.createQuery("SELECT cyclist FROM Cyclist cyclist WHERE cyclist.age <= 24", Cyclist.class)
                .getResultList();
    }

    @Override
    public List<Cyclist> getOldCyclist() {
        return em.createQuery("SELECT cyclist FROM Cyclist cyclist WHERE cyclist.age > 24", Cyclist.class)
                .getResultList();
    }

    @Override
    public List<Cyclist> getCyclistWithMostWins() {
        return em.createQuery(
                        "SELECT c FROM Cyclist c " +
                                "JOIN Race r ON c.id = r.cyclistID " +
                                "GROUP BY c.id, c.name " +
                                "ORDER BY COUNT(r.id) DESC, c.name", Cyclist.class)
                .setMaxResults(1)
                .getResultList();
    }


}
