package be.kdg.prog3.projectCycling.repository.jdbc;

import be.kdg.prog3.projectCycling.domain.Race;
import be.kdg.prog3.projectCycling.repository.RaceRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

@Repository
@Profile("jdbc")
public class RaceRepositoryImpl implements RaceRepository {
    private final JdbcTemplate jdbcTemplate;
    private final SimpleJdbcInsert raceInserter;

    public RaceRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.raceInserter = new SimpleJdbcInsert(jdbcTemplate)
                .withTableName("RACES")
                .usingGeneratedKeyColumns("ID");
    }

    public static Race mapRaceRow(ResultSet rs, int rowid) throws SQLException {
        Timestamp raceDateLocal = rs.getTimestamp("RACEDATE");

        LocalDate raceDate = null;
        if (raceDateLocal != null) {
            raceDate = raceDateLocal.toInstant()
                    .atZone(ZoneId.systemDefault())
                    .toLocalDate();
        }
        return new Race(rs.getInt("ID"),
                rs.getString("RACENAME"),
                raceDate,
                rs.getString("LOCATION"),
                rs.getString("CATEGORY"),
                rs.getInt("CYCLIST_ID"));
    }


    @Override
    public List<Race> findAll() {
        return jdbcTemplate.query("SELECT R.*, C.NAME " +
                "FROM RACES R " +
                "JOIN CYCLIST_RACE CR ON R.ID = CR.ID_Races " +
                "JOIN CYCLISTS C ON C.ID = CR.ID_Cyclist", RaceRepositoryImpl::mapRaceRow);
    }


    @Override
    public Race findById(int id) {
        return jdbcTemplate.queryForObject("SELECT * FROM RACES WHERE ID = ?", RaceRepositoryImpl::mapRaceRow, id);

    }
}
