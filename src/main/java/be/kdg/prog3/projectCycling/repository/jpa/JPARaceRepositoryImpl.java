package be.kdg.prog3.projectCycling.repository.jpa;

import be.kdg.prog3.projectCycling.domain.Race;
import be.kdg.prog3.projectCycling.repository.RaceRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Profile("jpa")
public class JPARaceRepositoryImpl implements RaceRepository {

    @PersistenceContext
    private EntityManager em;
    @Override
    public List<Race> findAll() {
        return em.createQuery("select race from Race race", Race.class)
                .getResultList();
    }

    @Override
    public Race findById(int id) {
        Race race = em.find(Race.class, id);
        return race;
    }
}
