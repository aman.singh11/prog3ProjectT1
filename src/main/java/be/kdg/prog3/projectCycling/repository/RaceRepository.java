package be.kdg.prog3.projectCycling.repository;

import be.kdg.prog3.projectCycling.domain.Race;

import java.util.List;

public interface RaceRepository {

    List<Race> findAll();

    Race findById(int id);

}
