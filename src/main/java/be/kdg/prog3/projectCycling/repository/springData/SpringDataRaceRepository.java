package be.kdg.prog3.projectCycling.repository.springData;

import be.kdg.prog3.projectCycling.domain.Race;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Profile("springData")
public interface SpringDataRaceRepository extends JpaRepository<Race, Integer> {
}
