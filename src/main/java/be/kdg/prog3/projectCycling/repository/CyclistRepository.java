package be.kdg.prog3.projectCycling.repository;

import be.kdg.prog3.projectCycling.domain.Cyclist;

import java.util.List;

public interface CyclistRepository {
    List<Cyclist> findAll();

    List<Cyclist> findAllWithTeamAndRace();

    Cyclist findById(int id);

    void createCyclist(Cyclist cyclist);

    void updateCyclist(Cyclist cyclist);

    void deleteCyclist(int id);

    List<Cyclist> getYoungCyclist();

    List<Cyclist> getOldCyclist();

    List<Cyclist> getCyclistWithMostWins();
}
