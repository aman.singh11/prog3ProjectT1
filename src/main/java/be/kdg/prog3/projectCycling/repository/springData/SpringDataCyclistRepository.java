package be.kdg.prog3.projectCycling.repository.springData;


import be.kdg.prog3.projectCycling.domain.Cyclist;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Profile("springData")
public interface SpringDataCyclistRepository extends JpaRepository<Cyclist, Integer> {
    List<Cyclist> findByAgeGreaterThan(int age);

    List<Cyclist> findByAgeBefore(int age);


    @Query(value = "SELECT c.*, COUNT(r.id) AS win_count " +
            "FROM cyclists c " +
            "JOIN cyclist_race cr ON c.id = cr.id_cyclist " +
            "JOIN races r ON cr.id_races = r.id " +
            "GROUP BY c.id, c.name " +
            "ORDER BY win_count DESC, c.NAME " +
            "LIMIT 1", nativeQuery = true)
    List<Cyclist> findCyclistWithMostWins();


}
