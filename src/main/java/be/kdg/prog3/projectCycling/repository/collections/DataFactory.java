package be.kdg.prog3.projectCycling.repository.collections;

import be.kdg.prog3.projectCycling.domain.Cyclist;
import be.kdg.prog3.projectCycling.domain.Race;
import be.kdg.prog3.projectCycling.domain.Team;
import org.springframework.stereotype.Component;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Component
public class DataFactory {


    public static List<Cyclist> cyclists = new ArrayList<>();
    public static List<Team> teams = new ArrayList<>();
    public static List<Race> races = new ArrayList<>();


    public static void seed() {
        Cyclist mvdp = new Cyclist(1, "Mathieu van der Poel", 28, "Dutch", 1.84, 2);
        Cyclist groves = new Cyclist(2, "Kaden Groves", 24, "Australian", 1.76, 2);
        Cyclist pogi = new Cyclist(3, "Tadej Pogačar", 25, "Slovenian", 1.76, 3);
        Cyclist roglic = new Cyclist(4, "Primoz Roglic", 33, "Slovenian", 1.77, 5);
        Cyclist froome = new Cyclist(5, "Chris Froome", 36, "British", 1.86, 1);
        Cyclist tarling = new Cyclist(6, "Joshua Tarling", 19, "British", 1.94, 1);
        Cyclist philipsen = new Cyclist(7, "Jasper Philipsen", 25, "Belgian", 1.76, 2);
        Cyclist wout = new Cyclist(8, "Wout Van Aert", 29, "Belgian", 1.9, 5);
        Cyclist ganna = new Cyclist(9, "Filippo Ganna ", 27, "Italian", 1.93, 1);
        Cyclist remco = new Cyclist(10, "Remco Evenepoel", 23, "Belgian", 1.71, 4);
        Cyclist egan = new Cyclist(11, "Egan Bernal", 24, "Colombian", 1.75, 1);
        Cyclist sagan = new Cyclist(12, "Peter Sagan", 31, "Slovakian", 1.84, 6);
        Cyclist sepp = new Cyclist(13, "Sepp Kuss", 29, "American", 1.82, 5);
        Cyclist jonas = new Cyclist(14, "Jonas Vingegaard", 26, "Danish", 1.75, 5);

        cyclists.add(mvdp);
        cyclists.add(groves);
        cyclists.add(pogi);
        cyclists.add(roglic);
        cyclists.add(froome);
        cyclists.add(tarling);
        cyclists.add(philipsen);
        cyclists.add(wout);
        cyclists.add(ganna);
        cyclists.add(remco);
        cyclists.add(egan);
        cyclists.add(sagan);
        cyclists.add(sepp);
        cyclists.add(jonas);


        Team ineos = new Team(1, "Ineos Grenadiers", "Ineos", "United Kingdom", "Pinarello", 2010);
        Team alpecin = new Team(2, "Alpecin–Deceuninck", "Alpecin", "Belgium", "Canyon", 2008);
        Team uae = new Team(3, "UAE Team Emirates", "Colnago", "UAE", "Colnago", 1999);
        Team quickStep = new Team(4, "Soudal Quick-Step", "Quick-Step", "Belgium", "Specialized", 2003);
        Team tjv = new Team(5, "Team Jumbo-Visma", "Jumbo", "Netherlands", "Cervelo", 1984);
        Team bora = new Team(6, "BORA-hansgrohe", "BORA", "Germany", "Specialized", 2010);

        teams.add(ineos);
        teams.add(alpecin);
        teams.add(uae);
        teams.add(quickStep);
        teams.add(tjv);
        teams.add(bora);


        Race tour = new Race(1, "Tour de France", LocalDate.of(2023, 7, 1), "France", Race.RaceCategory.GRAND_TOUR.name(), 14);
        Race giro = new Race(2, "Giro d'Italia", LocalDate.of(2023, 5, 8), "Italy", Race.RaceCategory.GRAND_TOUR.name(), 4);
        Race vuelta = new Race(3, "Vuelta a España", LocalDate.of(2023, 8, 19), "Spain", Race.RaceCategory.GRAND_TOUR.name(), 13);
        Race parisRoubaix = new Race(4, "Paris-Roubaix", LocalDate.of(2023, 4, 9), "France", Race.RaceCategory.MONUMENTS.name(), 1);
        Race amstel = new Race(5, "Amstel Gold Race", LocalDate.of(2023, 4, 9), "Netherlands", Race.RaceCategory.CLASSICS.name(), 3);
        Race tourOfFlanders = new Race(6, "Tour of Flanders", LocalDate.of(2023, 4, 9), "Belgium", Race.RaceCategory.MONUMENTS.name(), 3);
        Race msr = new Race(7, "Milan Sanremo", LocalDate.of(2023, 4, 9), "Italy", Race.RaceCategory.MONUMENTS.name(), 1);
        Race gentWevelgem = new Race(8, "Gent - Wevelgem", LocalDate.of(2023, 4, 9), "Belgium", Race.RaceCategory.CLASSICS.name(), 8);
        Race tourOfBelgium = new Race(9, "Tour of Belgium", LocalDate.of(2023, 4, 9), "Belgium", Race.RaceCategory.STAGES.name(), 1);
        Race uciWorldChamps = new Race(10, "UCI World Championship", LocalDate.of(2023, 4, 9), "Scotland", Race.RaceCategory.ONEDAY.name(), 1);

        races.add(tour);
        races.add(giro);
        races.add(vuelta);
        races.add(parisRoubaix);
        races.add(amstel);
        races.add(tourOfFlanders);
        races.add(msr);
        races.add(gentWevelgem);
        races.add(tourOfBelgium);
        races.add(uciWorldChamps);


        // Many-to-One Relationship: Assign professional cyclists to teams
        ineos.addCyclist(froome);
        ineos.addCyclist(tarling);
        ineos.addCyclist(ganna);
        ineos.addCyclist(egan);
        alpecin.addCyclist(mvdp);
        alpecin.addCyclist(philipsen);
        alpecin.addCyclist(groves);
        uae.addCyclist(pogi);
        quickStep.addCyclist(remco);
        tjv.addCyclist(jonas);
        tjv.addCyclist(sepp);
        tjv.addCyclist(wout);
        tjv.addCyclist(roglic);
        bora.addCyclist(sagan);

        // Many-to-Many Relationship: Assign professional cyclists to races

        tour.addCyclist(jonas);
        giro.addCyclist(roglic);
        vuelta.addCyclist(sepp);
        parisRoubaix.addCyclist(mvdp);
        amstel.addCyclist(pogi);
        tourOfFlanders.addCyclist(pogi);
        msr.addCyclist(mvdp);
        gentWevelgem.addCyclist(wout);
        tourOfBelgium.addCyclist(mvdp);
        uciWorldChamps.addCyclist(mvdp);

    }

}


