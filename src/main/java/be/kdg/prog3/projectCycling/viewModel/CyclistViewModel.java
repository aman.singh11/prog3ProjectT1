package be.kdg.prog3.projectCycling.viewModel;

import jakarta.validation.constraints.*;

public class CyclistViewModel {

    @NotNull(message = "ID is mandatory")
    int id;
    @NotBlank(message = "Name is mandatory")
    @Size(min = 3, max = 15, message = "Name must be between 3 and 15 characters")
    private String name;

    @NotNull(message = "Age is mandatory")
    @Min(value = 1, message = "Age must be greater than 0")
    private int age;

    @NotBlank(message = "Nationality is mandatory")
    private String nationality;

    @NotNull(message = "Height is mandatory")
    private double height;

    private int teamId;

    public CyclistViewModel() {
    }

    public CyclistViewModel(String name, int age, String nationality, double height) {
        this.name = name;
        this.age = age;
        this.nationality = nationality;
        this.height=height;
    }

    public CyclistViewModel(int id, String name, int age, String nationality, int teamId) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.nationality = nationality;
        this.teamId = teamId;
    }


    public int getTeamId() {
        return teamId;
    }

    public void setTeamId(int teamId) {
        this.teamId = teamId;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    @Override
    public String toString() {
        return "CyclistViewModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", nationality='" + nationality + '\'' +
                ", height=" + height +
                ", teamId=" + teamId +
                '}';
    }
}
