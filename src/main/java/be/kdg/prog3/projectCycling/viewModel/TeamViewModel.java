package be.kdg.prog3.projectCycling.viewModel;

import be.kdg.prog3.projectCycling.domain.Cyclist;
import jakarta.validation.constraints.*;


public class TeamViewModel {

    @NotNull(message = "ID is mandatory")
    private int id;
    @NotBlank(message = "Team name is mandatory")
    @Size(min = 3, max = 50, message = "Team name must be between 3 and 50 characters")
    private String teamName;

    @NotBlank(message = "Sponsor is mandatory")
    private String sponsor;

    @NotBlank(message = "Country is mandatory")
    private String country;

    @NotBlank(message = "Bike brand is mandatory")
    private String bikeBrand;

    @NotNull(message = "Founded year is mandatory")
    @Min(value = 1800, message = "Founded year must be greater than 1799")
    private int founded;

    private Cyclist cyclist;

    public TeamViewModel(String teamName, String sponsor, String country, String bikeBrand, int founded) {
        this.teamName = teamName;
        this.sponsor = sponsor;
        this.country = country;
        this.bikeBrand = bikeBrand;
        this.founded = founded;
    }

    public TeamViewModel(int id, String teamName, String sponsor, String country, String bikeBrand, int founded) {
        this.id = id;
        this.teamName = teamName;
        this.sponsor = sponsor;
        this.country = country;
        this.bikeBrand = bikeBrand;
        this.founded = founded;
    }

    public TeamViewModel() {

    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getSponsor() {
        return sponsor;
    }

    public void setSponsor(String sponsor) {
        this.sponsor = sponsor;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getBikeBrand() {
        return bikeBrand;
    }

    public void setBikeBrand(String bikeBrand) {
        this.bikeBrand = bikeBrand;
    }

    public int getFounded() {
        return founded;
    }

    public void setFounded(int founded) {
        this.founded = founded;
    }

    @Override
    public String toString() {
        return "TeamViewModel{" +
                "teamName='" + teamName + '\'' +
                ", sponsor='" + sponsor + '\'' +
                ", country='" + country + '\'' +
                ", bikeBrand='" + bikeBrand + '\'' +
                ", founded=" + founded +
                '}';
    }
}
