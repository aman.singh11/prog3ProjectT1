package be.kdg.prog3.projectCycling.controller;

import be.kdg.prog3.projectCycling.domain.Cyclist;
import be.kdg.prog3.projectCycling.domain.Team;
import be.kdg.prog3.projectCycling.exceptions.InvalidFilterValueException;
import be.kdg.prog3.projectCycling.service.CyclistService;
import be.kdg.prog3.projectCycling.service.TeamService;
import be.kdg.prog3.projectCycling.viewModel.CyclistViewModel;
import be.kdg.prog3.projectCycling.viewModel.TeamViewModel;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/")
@SessionAttributes("pageHistory")
public class CyclistControllerNew {
    private static final Logger LOG = LoggerFactory.getLogger(CyclistControllerNew.class);
    private final CyclistService cyclistService;
    private final TeamService teamService;


    @Autowired
    public CyclistControllerNew(CyclistService cyclistService, TeamService teamService) {
        this.cyclistService = cyclistService;
        this.teamService = teamService;
    }

    // Display the index page
    @GetMapping("/index")
    public String getIndex(HttpSession session) {
        addToPageHistory(session, "Index");
        LOG.info("Showing index page for language: ");
        return "index";
    }

    // Display the list of cyclists
    @GetMapping("/cyclists")
    public String showCyclists(Model model, HttpSession session) {
        addToPageHistory(session, "Cyclists");
        List<Cyclist> cyclists = cyclistService.getAllCyclists();
        List<Cyclist> cyclistsWithTeamAndRace = cyclistService.getAllCyclistsWithTeamAndRaces();
        model.addAttribute("cyclistWithTeamAndRace", cyclistsWithTeamAndRace);
        model.addAttribute("cyclists", cyclists);
        LOG.info("Showing cyclist View");
        return "cyclists-list";
    }

    // Filter cyclists based on a specified type
    @GetMapping("/filterCyclists")
    public String filterCyclists(@RequestParam(name = "filterType", defaultValue = "all") String filterType,
                                 Model model, HttpSession session) {
        addToPageHistory(session, " Cyclists");

        try {
            List<Cyclist> cyclists = switch (filterType) {
                case "youngCyclists" -> cyclistService.getYoungCyclist();
                case "oldCyclists" -> cyclistService.getOldCyclist();
                case "biggestWinner" -> cyclistService.getCyclistWithMostWins();
                default -> cyclistService.getAllCyclists(); // Default to all cyclists
            };

            List<Cyclist> cyclistsWithTeamAndRace = cyclistService.getAllCyclistsWithTeamAndRaces();
            model.addAttribute("cyclistWithTeamAndRace", cyclistsWithTeamAndRace);
            model.addAttribute("cyclists", cyclists);
            LOG.info("Showing " + filterType + " cyclist View");
            return "cyclists-list";
        } catch (InvalidFilterValueException e) {
            handleExceptionByFilterType(model, filterType, e);
            return "cyclists-list";
        }
    }


    // Handle exceptions based on filter type
    private void handleExceptionByFilterType(Model model, String filterType, InvalidFilterValueException e) {
        switch (filterType) {
            case "youngCyclists" -> model.addAttribute("youngCyclistError", e.getMessage());
            case "oldCyclists" -> model.addAttribute("oldCyclistError", e.getMessage());
            case "biggestWinner" -> model.addAttribute("biggestWinnerError", e.getMessage());
            default -> model.addAttribute(filterType + "Error", e.getMessage());
        }
    }

    // Display the form to add a new cyclist
    @GetMapping("/addcyclist")
    public String showAddCyclistForm(Model model) {
        model.addAttribute("newCyclist", new Cyclist());
        return "addNewCyclist";
    }


    // Add a new cyclist
    @PostMapping("/addcyclist")
    public String addCyclist(@Valid @ModelAttribute("newCyclist") CyclistViewModel cyclistViewModel,
                             BindingResult result, HttpSession session) {
        if (result.hasErrors()) {
            LOG.error("Form has errors: {}", result.getAllErrors());
            return "addNewCyclist";
        } else {
            LOG.info("Adding a new Cyclist");
            cyclistService.addCyclist(new Cyclist(cyclistViewModel.getName(),
                    cyclistViewModel.getAge(),
                    cyclistViewModel.getNationality(),
                    cyclistViewModel.getHeight()));
            addToPageHistory(session, "Add Cyclist");
            return "redirect:/cyclists";
        }
    }

    // Delete a cyclist
    @PostMapping("/delete-cyclist/{id}")
    public String deleteCyclist(@PathVariable int id, HttpSession session) {
        LOG.info("Deleting cyclist with ID: {}", id);
        cyclistService.deleteCyclist(id);
        addToPageHistory(session, "Delete Cyclist");
        return "redirect:/cyclists";
    }


    // Display the form to update a cyclist
    @GetMapping("/updatecyclist")
    public String showUpdateCyclistForm(@RequestParam(required = false) Integer id, Model model) {
        List<Cyclist> cyclists = cyclistService.getAllCyclists();
        model.addAttribute("cyclists", cyclists);
        List<Team> teams = teamService.getAllTeams();
        model.addAttribute("teams", teams);

        if (id != null) {
            Cyclist cyclist = cyclistService.getCyclistsById(id);

            CyclistViewModel cyclistViewModel = new CyclistViewModel();
            cyclistViewModel.setId(cyclist.getId());
            cyclistViewModel.setName(cyclist.getName());
            cyclistViewModel.setAge(cyclist.getAge());
            cyclistViewModel.setNationality(cyclist.getNationality());

            // Set teamId only when it's not null
            if (cyclist.getTeamId() != null) {
                cyclistViewModel.setTeamId(cyclist.getTeamId());
            }

            model.addAttribute("cyclist", cyclistViewModel);
        } else {
            model.addAttribute("cyclist", new CyclistViewModel());
        }

        return "updateCyclist";
    }


    // Display the form to update a team
    @GetMapping("/updateteam")
    public String showUpdateTeamForm(@RequestParam(required = false) Integer id, Model model) {
        List<Team> teams = teamService.getAllTeams();
        model.addAttribute("teams", teams);

        if (id != null) {
            Team team = teamService.findById(id);

            TeamViewModel teamViewModel = new TeamViewModel();
            teamViewModel.setId(team.getId());
            teamViewModel.setTeamName(team.getTeamName());
            teamViewModel.setSponsor(team.getSponsor());
            teamViewModel.setCountry(team.getCountry());
            teamViewModel.setBikeBrand(team.getBikeBrand());
            teamViewModel.setFounded(team.getFounded());

            model.addAttribute("team", teamViewModel);
        } else {
            model.addAttribute("team", new TeamViewModel());
        }

        return "updateTeam";
    }


    // Update a team
    @PostMapping("/updateteam")
    public String updateTeam(@ModelAttribute("team") TeamViewModel updatedTeamViewModel, HttpSession session, Model model) {
        LOG.info("Updating team with ID: {}", updatedTeamViewModel.getId());

        try {
            // Fetch the existing team from the database
            Team existingTeam = teamService.findById(updatedTeamViewModel.getId());
            // Update the fields
            existingTeam.setTeamName(updatedTeamViewModel.getTeamName());
            existingTeam.setCountry(updatedTeamViewModel.getCountry());
            existingTeam.setSponsor(updatedTeamViewModel.getSponsor());
            existingTeam.setBikeBrand(updatedTeamViewModel.getBikeBrand());
            existingTeam.setFounded(updatedTeamViewModel.getFounded());


            // Update the team in the service
            teamService.updateTeam(existingTeam);
            addToPageHistory(session, "Update Team");
            return "redirect:/teams";
        } catch (InvalidFilterValueException e) {
            model.addAttribute("teamIdError", e.getMessage());
        }
        List<Team> teams = teamService.getAllTeams();
        model.addAttribute("teams", teams);
        return "updateTeam";
    }


    // Update a cyclist
    @PostMapping("/updatecyclist")
    public String updateCyclist(@ModelAttribute("cyclist") CyclistViewModel updatedCyclistViewModel, HttpSession session, Model model) {
        LOG.info("Updating cyclist with ID: {}", updatedCyclistViewModel.getId());
        addToPageHistory(session, "Update Cyclist");
        // Fetch the existing cyclist from the database
        try {
            Cyclist existingCyclist = cyclistService.getCyclistsById(updatedCyclistViewModel.getId());
            // Update the fields
            existingCyclist.setName(updatedCyclistViewModel.getName());
            existingCyclist.setAge(updatedCyclistViewModel.getAge());
            existingCyclist.setNationality(updatedCyclistViewModel.getNationality());
            existingCyclist.setTeamId(updatedCyclistViewModel.getTeamId());

            // Fetch the existing team from the database
            Team existingTeam = teamService.findById(updatedCyclistViewModel.getTeamId());

            // Set the team for the cyclist
            existingCyclist.setTeam(existingTeam);
            // Update the cyclist in the service
            cyclistService.updateCyclist(existingCyclist);
            return "redirect:/cyclists";
        } catch (InvalidFilterValueException e) {
            if (e.getMessage().equals("ID cannot be found in the system!")) {
                model.addAttribute("idError", e.getMessage());
            } else {
                model.addAttribute("teamIdError", e.getMessage());
            }
            List<Cyclist> cyclists = cyclistService.getAllCyclists();
            model.addAttribute("cyclists", cyclists);
            List<Team> teams = teamService.getAllTeams();
            model.addAttribute("teams", teams);
            return "updateCyclist";
        }
    }

    // Display the list of teams
    @GetMapping("/teams")
    public String showTeams(Model model, HttpSession session) {
        addToPageHistory(session, "Teams");
        List<Team> teams = teamService.getAllTeams();
        List<Team> teamsWithRiders = teamService.getAllTeamWithRiders();
        model.addAttribute("teams", teams);
        model.addAttribute("teamsWithRiders", teamsWithRiders);
        LOG.info("Showing teams view");
        return "teams-list";
    }

    // Display the form to add a new team
    @GetMapping("/addteam")
    public String showAddTeamForm(Model model) {
        model.addAttribute("newTeam", new Team());
        return "addNewTeam";
    }

    // Add a new team
    @PostMapping("/addteam")
    public String addTeam(@Valid @ModelAttribute("newTeam") TeamViewModel teamViewModel,
                          BindingResult result, HttpSession session) {
        if (result.hasErrors()) {
            LOG.error("Form has errors: {}", result.getAllErrors());
            return "addNewTeam";
        } else {
            LOG.info("Adding a new Team");
            teamService.addTeam(new Team(teamViewModel.getTeamName(),
                    teamViewModel.getSponsor(),
                    teamViewModel.getCountry(),
                    teamViewModel.getBikeBrand(),
                    teamViewModel.getFounded()));
            addToPageHistory(session, "Add Team");
        }
        return "redirect:/teams";
    }

    // Delete a team
    @PostMapping("/delete-team/{id}")
    public String deleteTeam(@PathVariable int id, HttpSession session) {
        LOG.info("Deleting team with ID: {}", id);
        teamService.deleteTeam(id);
        addToPageHistory(session, "Delete Team");
        return "redirect:/teams";
    }

    // Display the session history page
    @GetMapping("/sessionHistory")
    public String showSessionHistory(Model model, HttpSession session) {
        @SuppressWarnings("unchecked")
        List<String> pageHistory = (List<String>) session.getAttribute("pageHistory");
        model.addAttribute("pageHistory", pageHistory);
        LOG.info("Showing session history page...");
        return "session-history";
    }

    // Add a page to the session history
    private void addToPageHistory(HttpSession session, String pageName) {
        @SuppressWarnings("unchecked")
        List<String> pageHistory = (List<String>) session.getAttribute("pageHistory");

        if (pageHistory == null) {
            pageHistory = new ArrayList<>();
        }
        LocalDateTime timestamp = LocalDateTime.now();
        String formattedTimestamp = timestamp.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        pageHistory.add(pageName + " - " + formattedTimestamp);
        LOG.info("Added " + pageName + " to the session history at " + formattedTimestamp);
        session.setAttribute("pageHistory", pageHistory);
    }
}
