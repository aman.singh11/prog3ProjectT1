package be.kdg.prog3.projectCycling;

import be.kdg.prog3.projectCycling.presenter.Menu;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class Prog3ProjectT1SpringApplication {

    public static void main(String[] args) {
        var context = SpringApplication.run(Prog3ProjectT1SpringApplication.class, args);
        context.getBean(Menu.class).startApplication();
        context.close();
    }
}
