package be.kdg.prog3.projectCycling.presenter;

import org.springframework.stereotype.Component;

import java.util.Scanner;


@Component
public class Menu {

    private final Presenter view;

    private static final Scanner scanner = new Scanner(System.in);


    public Menu(Presenter view) {
        this.view = view;
    }

    public void startApplication() {
        try {
            view.initializeData();
            boolean exit = false;

            do {
                view.printStartOptions();
                int mainChoice = view.getUserChoice();
                switch (mainChoice) {
                    case 0 -> {
                        view.exitProgram();
                        exit = true;
                    }
                    case 1 -> view.manageCyclists();
                    case 2 -> view.manageTeams();
                    default -> System.out.println("Invalid choice. Please select a valid option.");
                }
                System.out.println();
            } while (!exit);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            scanner.close();
        }
    }


}
