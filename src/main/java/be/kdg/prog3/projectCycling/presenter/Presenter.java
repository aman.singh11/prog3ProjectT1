package be.kdg.prog3.projectCycling.presenter;

import be.kdg.prog3.projectCycling.domain.Cyclist;
import be.kdg.prog3.projectCycling.domain.Race;
import be.kdg.prog3.projectCycling.repository.collections.DataFactory;
import be.kdg.prog3.projectCycling.domain.Team;
import org.springframework.stereotype.Component;
import be.kdg.prog3.projectCycling.json.JsonDataSaver;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class Presenter {

    private static final Scanner scanner = new Scanner(System.in);


    public void printStartOptions() {
        System.out.println("----------------------------------------------");
        System.out.println("            WELCOME TO CYCLE MANAGER          ");
        System.out.println("----------------------------------------------");
        System.out.println("Choose an option:");
        System.out.println("1. Manage Cyclists");
        System.out.println("2. Manage Teams");
        System.out.println("0. Quit");
        System.out.print("Enter your choice: ");
    }

    public void printMenuTeams() {
        System.out.println("What would you like to do?");
        System.out.println("==========================");
        System.out.println("0) Quit");
        System.out.println("1) Show all teams");
        System.out.println("2) Show teams by sponsor and/or country and/or foundation year");
        System.out.println("3) Add a team");
        System.out.println("4) Update Team");
        System.out.println("5) Delete Team");
        System.out.println("6) Show team details");
        System.out.print("Choice (0-6): ");
    }

    public void printMenuCyclists() {
        System.out.println("What would you like to do?");
        System.out.println("==========================");
        System.out.println("0) Quit");
        System.out.println("1) Show all cyclists");
        System.out.println("2) Show youngCyclist");
        System.out.println("3) Show oldCyclist");
        System.out.println("4) Show biggestWinner");
        System.out.println("5) Show cyclists by nationality");
        System.out.println("6) Add a cyclist");
        System.out.println("7) Update Cyclist");
        System.out.println("8) Delete Cyclist");
        System.out.println("9) Show cyclist details");
        System.out.print("Choice (0-9): ");
    }

    public void exitProgram() {
        System.out.println("Goodbye!");
        System.exit(0);
    }

    public void initializeData() {
        DataFactory.seed();
    }


    public int getUserChoice() {
        int choice = -1;
        try {
            choice = scanner.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Invalid input. Please enter a number.");
            scanner.nextLine();
        }
        scanner.nextLine();
        return choice;
    }


    public void manageCyclists() {
        boolean exitCyclists = false;

        do {
            printMenuCyclists();
            int choice = getUserChoice();

            switch (choice) {
                case 0 -> exitCyclists = true;
                case 1 -> showAllCyclists();
                case 2 -> {
                    List<Cyclist> youngCyclists = getYoungCyclist();
                    showCyclistList(youngCyclists);
                }
                case 3 -> {
                    List<Cyclist> oldCyclists = getOldCyclist();
                    showCyclistList(oldCyclists);
                }
                case 4 -> {
                    List<Cyclist> biggestWinner = getCyclistWithMostWins();
                    showCyclistList(biggestWinner);
                }
                case 5 -> showCyclistsByNationality();
                case 6 -> addCyclist();
                case 7 -> updateCyclist();
                case 8 -> deleteCyclist();
                case 9 -> showCyclistDetail();
                default -> System.out.println("Invalid choice. Please select a valid option.");
            }

            System.out.println();
        } while (!exitCyclists);
    }

    public void manageTeams() {
        boolean exitTeams = false;

        do {
            printMenuTeams();
            int choice = getUserChoice();

            switch (choice) {
                case 0 -> exitTeams = true;
                case 1 -> showAllTeams();
                case 2 -> showTeamsByFilters();
                case 3 -> addTeam();
                case 4 -> updateTeam();
                case 5 -> deleteTeam();
                case 6 -> showTeamDetail();
                default -> System.out.println("Invalid choice. Please select a valid option.");
            }

            System.out.println();
        } while (!exitTeams);
    }


    public void showAllCyclists() {
        List<Cyclist> cyclists = DataFactory.cyclists;
        showCyclistList(cyclists);
    }


    public void showCyclistList(List<Cyclist> cyclists) {
        System.out.println();
        System.out.println("All cyclists");
        System.out.println("._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._.");
        System.out.println();
        for (Cyclist cyclist : cyclists) {
            System.out.println(cyclist);
        }
        System.out.println();
        JsonDataSaver.saveDataToJson("cyclists.json", cyclists); // Save cyclists data to JSON
        System.out.println("._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._.");

    }

    public void addCyclist() {
        System.out.print("Enter cyclist ID: ");
        int cyclistId = Integer.parseInt(scanner.nextLine().trim());
        System.out.print("Enter cyclist name: ");
        String name = scanner.nextLine().trim();
        System.out.print("Enter cyclist age: ");
        int age = Integer.parseInt(scanner.nextLine().trim());
        System.out.print("Enter cyclist nationality: ");
        String nationality = scanner.nextLine().trim();
        System.out.print("Enter cyclist height: ");
        int height = Integer.parseInt(scanner.nextLine().trim());

        Cyclist newCyclist = new Cyclist(cyclistId, name, age, nationality, height);
        DataFactory.cyclists.add(newCyclist);

        System.out.println("Cyclist added successfully!");
    }

    public void addTeam() {
        System.out.print("Enter team ID: ");
        int teamId = Integer.parseInt(scanner.nextLine().trim());
        System.out.print("Enter team name: ");
        String teamName = scanner.nextLine().trim();
        System.out.print("Enter team sponsor: ");
        String sponsor = scanner.nextLine().trim();
        System.out.print("Enter team country: ");
        String country = scanner.nextLine().trim();
        System.out.print("Enter team foundation year: ");
        int foundationYear = Integer.parseInt(scanner.nextLine().trim());
        System.out.print("Enter team bike brand: ");
        String bikeBrand = scanner.nextLine().trim();

        Team newTeam = new Team(teamId, teamName, sponsor, country, bikeBrand, foundationYear);
        DataFactory.teams.add(newTeam);

        System.out.println("Team added successfully!");
    }


    public void updateCyclist() {
        showAllCyclists();
        int cyclistId;
        Cyclist cyclistToUpdate;

        do {
            System.out.print("Enter cyclist ID to update: ");
            cyclistId = Integer.parseInt(scanner.nextLine().trim());

            // Find the cyclist with the specified ID
            int finalCyclistId = cyclistId;
            cyclistToUpdate = DataFactory.cyclists.stream()
                    .filter(cyclist -> cyclist.getId() == finalCyclistId)
                    .findFirst()
                    .orElse(null);

            if (cyclistToUpdate == null) {
                System.out.println("Cyclist with ID " + cyclistId + " not found. Please enter a valid ID.");
            }
        } while (cyclistToUpdate == null);
        System.out.print("Enter updated cyclist name: ");
        String name = scanner.nextLine().trim();
        System.out.print("Enter updated cyclist age: ");
        int age = Integer.parseInt(scanner.nextLine().trim());
        System.out.print("Enter updated cyclist nationality: ");
        String nationality = scanner.nextLine().trim();
        int teamId;
        Team team;

        do {
            System.out.print("Enter updated cyclist team ID: ");
            teamId = Integer.parseInt(scanner.nextLine().trim());

            // Find the team with the specified ID
            int finalTeamId = teamId;
            team = DataFactory.teams.stream()
                    .filter(t -> t.getId() == finalTeamId)
                    .findFirst()
                    .orElse(null);

            if (team == null) {
                System.out.println("Team with ID " + teamId + " not found. Please enter a valid team ID.");
            }
        } while (team == null);
        cyclistToUpdate.setName(name);
        cyclistToUpdate.setAge(age);
        cyclistToUpdate.setNationality(nationality);
        cyclistToUpdate.setTeamId(teamId);
        System.out.println("Cyclist updated successfully!");
    }


    public void updateTeam() {
        showAllTeams();
        int teamId;
        Team teamToUpdate;

        do {
            System.out.print("Enter team ID to update: ");
            teamId = Integer.parseInt(scanner.nextLine().trim());

            // Find the team with the specified ID
            int finalTeamId = teamId;
            teamToUpdate = DataFactory.teams.stream()
                    .filter(team -> team.getId() == finalTeamId)
                    .findFirst()
                    .orElse(null);

            if (teamToUpdate == null) {
                System.out.println("Team with ID " + teamId + " not found. Please enter a valid ID.");
            }
        } while (teamToUpdate == null);
        System.out.print("Enter updated team name: ");
        String teamName = scanner.nextLine().trim();
        System.out.print("Enter updated team sponsor: ");
        String sponsor = scanner.nextLine().trim();
        System.out.print("Enter updated team country: ");
        String country = scanner.nextLine().trim();
        System.out.print("Enter updated team foundation year: ");
        int foundationYear = Integer.parseInt(scanner.nextLine().trim());
        System.out.print("Enter updated team bike brand: ");
        String bikeBrand = scanner.nextLine().trim();

        teamToUpdate.setTeamName(teamName);
        teamToUpdate.setSponsor(sponsor);
        teamToUpdate.setCountry(country);
        teamToUpdate.setFounded(foundationYear);
        teamToUpdate.setBikeBrand(bikeBrand);
        System.out.println("Team updated successfully!");
    }


    public void showCyclistsByNationality() {
        List<Cyclist> cyclistsByNationality = getCyclistsByNationality();
        showCyclistsByNationalityList(cyclistsByNationality);
    }

    public void showCyclistsByNationalityList(List<Cyclist> cyclists) {
        System.out.print("Enter nationality or leave blank: ");
        String nationalityFilter = scanner.nextLine().trim();
        System.out.println();
        System.out.println("Cyclists by nationality: " + nationalityFilter);
        System.out.println("._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._.");
        System.out.println();

        // Filter cyclists by nationality
        List<Cyclist> filteredCyclists = new ArrayList<>();
        for (Cyclist cyclist : cyclists) {
            if (nationalityFilter.isEmpty() || cyclist.getNationality().equalsIgnoreCase(nationalityFilter)) {
                System.out.println(cyclist);
                filteredCyclists.add(cyclist);
            }
        }
        System.out.println("Cyclist with nationality: " + nationalityFilter + " not found.");
        System.out.println();
        // Save filtered cyclists data to JSON
        JsonDataSaver.saveDataToJson("cyclists.json", filteredCyclists);
        System.out.println("._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._.");
    }


    public void showAllTeams() {
        List<Team> teams = DataFactory.teams;
        showAllTeamsLists(teams);
    }

    public void showAllTeamsLists(List<Team> teams) {
        System.out.println();
        System.out.println("All teams");
        System.out.println("=================================================================================");
        System.out.println();
        for (Team team : teams) {
            System.out.println(team);
        }
        System.out.println();
        JsonDataSaver.saveDataToJson("teams.json", teams); // Save teams data to JSON
        System.out.println("=================================================================================");
    }


    public void showTeamsByFilters() {
        List<Team> teams = getTeamsFiltered();
        showTeamsByFiltersList(teams);
    }

    public void showTeamsByFiltersList(List<Team> teams) {
        System.out.print("Enter sponsor or leave blank: ");
        String sponsorFilter = scanner.nextLine().trim();
        System.out.print("Enter country or leave blank: ");
        String countryFilter = scanner.nextLine().trim();
        System.out.print("Enter foundation year or leave blank: ");
        String yearFilterInput = scanner.nextLine().trim();
        int yearFilter = -1;
        if (!yearFilterInput.isEmpty()) {
            try {
                yearFilter = Integer.parseInt(yearFilterInput);
            } catch (NumberFormatException e) {
                System.out.println("Invalid year format");
            }
        }
        System.out.println();
        System.out.println("Teams by sponsor and/or country and/or foundation year");
        System.out.println("=================================================================================");
        System.out.println();
        // Filter teams by sponsor, country, and foundation year
        List<Team> filteredTeams = new ArrayList<>();
        for (Team team : teams) {
            if ((sponsorFilter.isEmpty() || team.getSponsor().equalsIgnoreCase(sponsorFilter)) && (countryFilter.isEmpty() || team.getCountry().equalsIgnoreCase(countryFilter)) && (yearFilter == -1 || team.getFounded() == yearFilter)) {
                System.out.println(team);
                filteredTeams.add(team);
            }
        }
        System.out.println();
        // Save filtered teams data to JSON
        JsonDataSaver.saveDataToJson("teams.json", filteredTeams);
        System.out.println("=================================================================================");
    }

    public List<Cyclist> getCyclistsByNationality() {
        List<Cyclist> cyclists = DataFactory.cyclists;
        return cyclists.stream()
                .collect(Collectors.groupingBy(Cyclist::getNationality))
                .values()
                .stream()
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }

    public List<Team> getTeamsFiltered() {
        return DataFactory.teams;

    }

    public void deleteCyclist() {
        System.out.print("Enter cyclist ID to delete: ");
        int cyclistId = Integer.parseInt(scanner.nextLine().trim());

        // Remove the cyclist with the specified ID
        boolean removed = DataFactory.cyclists.removeIf(cyclist -> cyclist.getId() == cyclistId);

        if (removed) {
            System.out.println("Cyclist deleted successfully!");
        } else {
            System.out.println("Cyclist with ID " + cyclistId + " not found.");
        }
    }

    public void deleteTeam() {
        System.out.print("Enter team ID to delete: ");
        int teamId = Integer.parseInt(scanner.nextLine().trim());

        // Remove the team with the specified ID
        boolean removed = DataFactory.teams.removeIf(team -> team.getId() == teamId);

        if (removed) {
            System.out.println("Team deleted successfully!");
        } else {
            System.out.println("Team with ID " + teamId + " not found.");
        }
    }


    public void showCyclistDetail() {
        System.out.print("Enter cyclist ID to show details: ");
        int cyclistId = Integer.parseInt(scanner.nextLine().trim());

        // Find the cyclist with the specified ID
        Cyclist cyclist = DataFactory.cyclists.stream()
                .filter(c -> c.getId() == cyclistId)
                .findFirst()
                .orElse(null);

        if (cyclist != null) {
            System.out.println("Cyclist details:");
            System.out.println(cyclist);

            Team linkedTeam = DataFactory.teams.stream()
                    .filter(t -> t.getId() == cyclist.getTeamId())
                    .findFirst()
                    .orElse(null);

            if (linkedTeam != null) {
                System.out.println("Linked Team details:");
                System.out.println(linkedTeam);
            } else {
                System.out.println("Linked Team not found.");
            }

            List<Race> linkedRaces = DataFactory.races.stream()
                    .filter(race -> race.getCyclists().contains(cyclist))
                    .toList();

            if (!linkedRaces.isEmpty()) {
                System.out.println("Linked Races details:");
                linkedRaces.forEach(System.out::println);
            } else {
                System.out.println("No linked races found.");
            }

        } else {
            System.out.println("Cyclist with ID " + cyclistId + " not found.");
        }
    }

    public void showTeamDetail() {
        System.out.print("Enter team ID to show details: ");
        int teamId = Integer.parseInt(scanner.nextLine().trim());

        // Find the team with the specified ID
        Team team = DataFactory.teams.stream()
                .filter(t -> t.getId() == teamId)
                .findFirst()
                .orElse(null);

        if (team != null) {
            System.out.println("Team details:");
            System.out.println(team);

            List<Cyclist> linkedCyclists = DataFactory.cyclists.stream()
                    .filter(cyclist -> cyclist.getTeamId() == teamId)
                    .toList();

            if (!linkedCyclists.isEmpty()) {
                System.out.println("Linked Cyclists details:");
                linkedCyclists.forEach(System.out::println);
            } else {
                System.out.println("No linked cyclists found.");
            }

        } else {
            System.out.println("Team with ID " + teamId + " not found.");
        }
    }

    public List<Cyclist> getYoungCyclist() {
        return DataFactory.cyclists.stream()
                .filter(cyclist -> cyclist.getAge() <= 24)
                .toList();
    }


    public List<Cyclist> getOldCyclist() {
        return DataFactory.cyclists.stream()
                .filter(cyclist -> cyclist.getAge() > 24)
                .toList();
    }


    public List<Cyclist> getCyclistWithMostWins() {
        return DataFactory.cyclists.stream()
                .max(Comparator.comparingInt(cyclist ->
                        DataFactory.races.stream()
                                .filter(race -> race.getCyclists().contains(cyclist))
                                .mapToInt(race -> 1)
                                .sum()))
                .map(List::of)
                .orElse(List.of());
    }

}





