package be.kdg.prog3.projectCycling.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "RACES")
public class Race {

    public enum RaceCategory {
        ONEDAY, GRAND_TOUR, CLASSICS, MONUMENTS, STAGES, DEFAULT
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "RACENAME")
    private String raceName;

    @Column(name = "RACEDATE")
    private LocalDate date;

    @Column(name = "LOCATION")
    private String location;

    @Column(name = "CATEGORY")
    private String category;

    @Column(name = "CYCLIST_ID")
    private Integer cyclistID;

    @JsonIgnore
    @ManyToMany(cascade = {CascadeType.DETACH, CascadeType.MERGE,
            CascadeType.PERSIST, CascadeType.REFRESH}, fetch = FetchType.EAGER)
    @JoinTable(
            name = "CYCLIST_RACE",
            joinColumns = @JoinColumn(name = "ID_Races"),
            inverseJoinColumns = @JoinColumn(name = "ID_Cyclist")
    )
    private List<Cyclist> cyclists; // Many-to-many relationship with Cyclist

    public Race(String raceName,LocalDate date, String location, String category) {
        this.raceName = raceName;
        this.date = date;
        this.location = location;
        this.category = category;
        this.cyclists = new ArrayList<>();
    }

    public Race() {
        this.cyclists = new ArrayList<>();
    }

    public Race(int id, String raceName, LocalDate date, String location, String category, Integer cyclistID) {
        this.id = id;
        this.raceName = raceName;
        this.date = date;
        this.location = location;
        this.category = category;
        this.cyclistID = cyclistID;
        this.cyclists = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRaceName() {
        return raceName;
    }

    public void setRaceName(String raceName) {
        this.raceName = raceName;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Integer getCyclistID() {
        return cyclistID;
    }

    public void setCyclistID(Integer cyclistID) {
        this.cyclistID = cyclistID;
    }

    public void setCyclists(List<Cyclist> cyclists) {
        this.cyclists = cyclists;
    }

    public List<Cyclist> getCyclists() {
        return cyclists;
    }

    public void addCyclist(Cyclist cyclist) {
        this.cyclists.add(cyclist);
    }

    @Override
    public String toString() {
        return "Race{" +
                "id=" + id +
                ", raceName='" + raceName + '\'' +
                ", date=" + date +
                ", location='" + location + '\'' +
                ", category='" + category + '\'' +
                '}';
    }
}
