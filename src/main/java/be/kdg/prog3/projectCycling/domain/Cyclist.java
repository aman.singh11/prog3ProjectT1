package be.kdg.prog3.projectCycling.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "CYCLISTS")
public class Cyclist {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "NAME")
    private String name;
    @Column(name = "AGE")
    private int age;
    @Column(name = "NATIONALITY")
    private String nationality;

    @Column(name = "HEIGHT")
    private double height;

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }


    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE,
            CascadeType.PERSIST, CascadeType.REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = "TEAM_ID")
    private Team team; // Many-to-one relationship with Team

    @Column(name = "TEAM_ID", nullable = true, insertable = false, updatable = false)
    private Integer teamId;


    @JsonIgnore
    @ManyToMany(mappedBy = "cyclists")
    private List<Race> races = new ArrayList<>(); // Many-to-many relationship with Race

    public Cyclist() {
    }

    public Cyclist(int id, String name, int age, String nationality, double height) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.nationality = nationality;
        this.height = height;
    }
    public Cyclist(int id, String name, int age, String nationality, double height, Team team, Integer teamId, List<Race> races) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.nationality = nationality;
        this.height = height;
        this.team = team;
        this.teamId = teamId;
        this.races = races;
    }

    public Cyclist(int id, String name, int age, String nationality, int teamId) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.nationality = nationality;
        this.teamId = teamId;
        this.races = new ArrayList<>();
    }

    public Cyclist(int id, String name, int age, String nationality, double height, int teamId) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.nationality = nationality;
        this.height = height;
        this.teamId=teamId;
    }

    public Cyclist(String name, int age, String nationality, int teamId) {
        this.name = name;
        this.age = age;
        this.nationality = nationality;
        this.teamId = teamId;
        this.races = new ArrayList<>();
    }

    public Cyclist(String name, int age, String nationality, double height) {
        this.name = name;
        this.age = age;
        this.nationality = nationality;
        this.height = height;
        this.races = new ArrayList<>();
    }


    public Integer getTeamId() {
        return teamId;
    }

    public void setTeamId(int teamId) {
        this.teamId = teamId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeamId(Integer teamId) {
        this.teamId = teamId;
    }

    public void setRaces(List<Race> races) {
        this.races = races;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getNationality() {
        return nationality;
    }

    public Team getTeams() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public List<Race> getRaces() {
        return races;
    }

    public void addRace(Race race) {
        races.add(race);
        race.addCyclist(this);
    }

    @Override
    public String toString() {
        return "Cyclist{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", nationality='" + nationality + '\'' +
                ", height=" + height +
                '}';
    }
}


