package be.kdg.prog3.projectCycling.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "TEAMS")
public class Team {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "TEAMNAME")
    private String teamName;
    @Column(name = "SPONSOR")
    private String sponsor;
    @Column(name = "COUNTRY")
    private String country;
    @Column(name = "BIKEBRAND")
    private String bikeBrand;
    @Column(name = "FOUNDED")
    private int founded;
    @JsonIgnore
    @OneToMany(cascade = {CascadeType.DETACH, CascadeType.MERGE,
            CascadeType.PERSIST, CascadeType.REFRESH}, mappedBy = "team")
    private List<Cyclist> cyclists; // One-to-many relationship with Cyclist

    public Team(int id, String teamName, String sponsor, String country, String bikeBrand, int founded) {
        this.id = id;
        this.teamName = teamName;
        this.sponsor = sponsor;
        this.country = country;
        this.bikeBrand = bikeBrand;
        this.founded = founded;
        this.cyclists = new ArrayList<>();
    }


    public Team(String teamName, String sponsor, String country, String bikeBrand, int founded) {
        this.teamName = teamName;
        this.sponsor = sponsor;
        this.country = country;
        this.bikeBrand = bikeBrand;
        this.founded = founded;
        this.cyclists = new ArrayList<>();
    }


    public Team() {
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public void setSponsor(String sponsor) {
        this.sponsor = sponsor;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setBikeBrand(String bikeBrand) {
        this.bikeBrand = bikeBrand;
    }

    public void setFounded(int founded) {
        this.founded = founded;
    }

    public String getBikeBrand() {
        return bikeBrand;
    }

    public int getFounded() {
        return founded;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTeamName() {
        return teamName;
    }

    public String getSponsor() {
        return sponsor;
    }

    public String getCountry() {
        return country;
    }

    public List<Cyclist> getCyclists() {
        return cyclists;
    }

    public void addCyclist(Cyclist cyclist) {
        cyclists.add(cyclist);
        cyclist.setTeam(this); // Setting the cyclist's team to this team
    }

    @Override
    public String toString() {
        return "Team{" +
                "id=" + getId() +
                ", teamName='" + teamName + '\'' +
                ", sponsor='" + sponsor + '\'' +
                ", country='" + country + '\'' +
                ", bikeBrand='" + bikeBrand + '\'' +
                ", founded=" + founded +
                '}';
    }
}