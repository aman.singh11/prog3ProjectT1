package be.kdg.prog3.projectCycling.exceptions;

public class DataAccessException extends RuntimeException {
    public DataAccessException(String message){
        super(message);
    }
}
