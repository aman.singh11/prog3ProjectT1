package be.kdg.prog3.projectCycling.exceptions;

public class InvalidFilterValueException extends RuntimeException {
    public InvalidFilterValueException(String message) {
        super(message);
    }
}
