package be.kdg.prog3.projectCycling.service;

import be.kdg.prog3.projectCycling.domain.Cyclist;

import java.util.List;

public interface CyclistService {

    List<Cyclist> getAllCyclists();
    List<Cyclist> getAllCyclistsWithTeamAndRaces();

    Cyclist getCyclistsById(int id);

    Cyclist addCyclist(Cyclist cyclist);
    Cyclist updateCyclist (Cyclist cyclist);

    void deleteCyclist(int id);

    List<Cyclist> getYoungCyclist();

    List<Cyclist> getOldCyclist();

    List<Cyclist> getCyclistWithMostWins();
}
