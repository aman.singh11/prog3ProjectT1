package be.kdg.prog3.projectCycling.service;


import be.kdg.prog3.projectCycling.domain.Cyclist;
import be.kdg.prog3.projectCycling.domain.Team;

import java.util.List;

public interface TeamService {

    List<Team> getAllTeams();
    List<Team> getAllTeamWithRiders();

    void addTeam(Team team);

    Team updateTeam (Team team);

    Team findById(int id);

    void deleteTeam(int id);

}

