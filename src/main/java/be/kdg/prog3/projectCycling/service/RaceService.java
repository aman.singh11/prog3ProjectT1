package be.kdg.prog3.projectCycling.service;

import be.kdg.prog3.projectCycling.domain.Cyclist;
import be.kdg.prog3.projectCycling.domain.Race;

import java.util.List;

public interface RaceService {
    List<Race> getAllRaces();

    Race getRaceById(int id);
}
