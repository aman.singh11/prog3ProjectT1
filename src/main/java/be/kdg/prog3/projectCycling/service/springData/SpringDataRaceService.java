package be.kdg.prog3.projectCycling.service.springData;


import be.kdg.prog3.projectCycling.domain.Race;
import be.kdg.prog3.projectCycling.repository.springData.SpringDataRaceRepository;
import be.kdg.prog3.projectCycling.service.RaceService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Profile("springData")
public class SpringDataRaceService implements RaceService {

    private final SpringDataRaceRepository springDataRaceRepository;

    public SpringDataRaceService(SpringDataRaceRepository springDataRaceRepository) {
        this.springDataRaceRepository = springDataRaceRepository;
    }

    @Override
    public List<Race> getAllRaces() {
        return springDataRaceRepository.findAll();
    }

    @Override
    public Race getRaceById(int id) {
        return springDataRaceRepository.findById(id).get();
    }
}
