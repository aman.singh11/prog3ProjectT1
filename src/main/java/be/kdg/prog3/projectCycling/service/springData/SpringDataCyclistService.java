package be.kdg.prog3.projectCycling.service.springData;


import be.kdg.prog3.projectCycling.domain.Cyclist;
import be.kdg.prog3.projectCycling.domain.Race;
import be.kdg.prog3.projectCycling.exceptions.InvalidFilterValueException;
import be.kdg.prog3.projectCycling.repository.springData.SpringDataCyclistRepository;
import be.kdg.prog3.projectCycling.service.CyclistService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Profile("springData")
public class SpringDataCyclistService implements CyclistService {
    private static final Logger logger = LoggerFactory.getLogger(SpringDataCyclistService.class);

    private final SpringDataCyclistRepository springDataCyclistRepository;

    public SpringDataCyclistService(SpringDataCyclistRepository springDataCyclistRepository) {
        this.springDataCyclistRepository = springDataCyclistRepository;
    }


    @Override
    public List<Cyclist> getAllCyclists() {
        return springDataCyclistRepository.findAll();
    }


    @Override
    public List<Cyclist> getAllCyclistsWithTeamAndRaces() {
        return springDataCyclistRepository.findAll();
    }

    @Override
    public Cyclist getCyclistsById(int id) {
        try {
            Optional<Cyclist> optionalCyclist = springDataCyclistRepository.findById(id);

            if (optionalCyclist.isPresent()) {
                return optionalCyclist.get();
            } else {
                throw new InvalidFilterValueException("ID cannot be found in the system!");
            }

        } catch (NullPointerException e) {
            throw new InvalidFilterValueException("ID cannot be found in the system!");
        }
    }


    @Override
    public Cyclist addCyclist(Cyclist cyclist) {
        return springDataCyclistRepository.save(cyclist);
    }

    @Override
    public Cyclist updateCyclist(Cyclist cyclist) {
        return springDataCyclistRepository.save(cyclist);
    }

    @Override
    public void deleteCyclist(int id) {
        Optional<Cyclist> cyclistOptional = springDataCyclistRepository.findById(id);

        if (cyclistOptional.isPresent()) {
            Cyclist cyclist = cyclistOptional.get();

            // Set the teamId to null in the associated races
            for (Race race : cyclist.getRaces()) {
                race.setCyclistID(null);
                race.setCyclists(null);
            }

            // Now, delete the cyclist
            springDataCyclistRepository.deleteById(id);
        }
    }


    @Override
    public List<Cyclist> getYoungCyclist() {
        try {
            List<Cyclist> youngCyclists = springDataCyclistRepository.findByAgeBefore(25);
            handleEmptyResult(youngCyclists, "No young cyclists found.");
            return youngCyclists;
        } catch (Exception e) {
            logger.error("An error occurred while retrieving young cyclists.", e);
            throw new InvalidFilterValueException("Error retrieving young cyclists.");
        }
    }

    @Override
    public List<Cyclist> getOldCyclist() {
        try {
            List<Cyclist> oldCyclists = springDataCyclistRepository.findByAgeGreaterThan(25);
            handleEmptyResult(oldCyclists, "No old cyclists found.");
            return oldCyclists;
        } catch (Exception e) {
            logger.error("An error occurred while retrieving old cyclists.", e);
            throw new InvalidFilterValueException("Error retrieving old cyclists.");
        }
    }

    @Override
    public List<Cyclist> getCyclistWithMostWins() {
        try {
            List<Cyclist> cyclistsWithMostWins = springDataCyclistRepository.findCyclistWithMostWins();
            handleEmptyResult(cyclistsWithMostWins, "No cyclists with most wins found.");
            return cyclistsWithMostWins;
        } catch (Exception e) {
            logger.error("An error occurred while retrieving cyclists with most wins.", e);
            throw new InvalidFilterValueException("Error retrieving cyclists with most wins.");
        }
    }

    private void handleEmptyResult(List<?> resultList, String message) {
        if (resultList.isEmpty()) {
            logger.warn(message);
            throw new InvalidFilterValueException(message);
        }
    }
}
