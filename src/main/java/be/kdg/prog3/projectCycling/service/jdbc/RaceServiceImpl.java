package be.kdg.prog3.projectCycling.service.jdbc;

import be.kdg.prog3.projectCycling.domain.Race;
import be.kdg.prog3.projectCycling.repository.RaceRepository;
import be.kdg.prog3.projectCycling.service.RaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Profile({"jdbc", "jpa"})
public class RaceServiceImpl implements RaceService {

    private final RaceRepository raceRepository;

    @Autowired
    public RaceServiceImpl(RaceRepository raceRepository) {
        this.raceRepository = raceRepository;
    }

    @Override
    public List<Race> getAllRaces() {
        return raceRepository.findAll();
    }

    @Override
    public Race getRaceById(int id) {
        return raceRepository.findById(id);
    }
}
