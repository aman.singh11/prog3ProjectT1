package be.kdg.prog3.projectCycling.service.jdbc;

import be.kdg.prog3.projectCycling.domain.Team;
import be.kdg.prog3.projectCycling.exceptions.InvalidFilterValueException;
import be.kdg.prog3.projectCycling.repository.TeamRepository;
import be.kdg.prog3.projectCycling.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@Profile({"jdbc", "jpa"})
public class TeamServiceImpl implements TeamService {


    private final TeamRepository teamRepository;


    @Autowired
    public TeamServiceImpl(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }

    @Override
    public List<Team> getAllTeams() {
        return teamRepository.findAll();
    }

    @Override
    public List<Team> getAllTeamWithRiders() {
        return teamRepository.findAllWithCyclist();
    }

    @Override
    public void addTeam(Team team) {
        teamRepository.createTeam(team);
    }

    @Override
    public Team updateTeam(Team team) {
        teamRepository.updateTeam(team);
        return team;
    }

    @Override
    public Team findById(int id) {
        try {
            Team team = teamRepository.findById(id);
            if (team == null || team.getId() == 0)
                throw new InvalidFilterValueException("Team with this id doesn't exist");
            return team;
        } catch (EmptyResultDataAccessException e) {
            throw new InvalidFilterValueException("Team with this id doesn't exist");
        }
    }

    @Override
    public void deleteTeam(int id) {
        teamRepository.deleteTeam(id);
    }

}
