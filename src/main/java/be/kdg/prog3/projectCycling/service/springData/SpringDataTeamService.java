package be.kdg.prog3.projectCycling.service.springData;


import be.kdg.prog3.projectCycling.domain.Cyclist;
import be.kdg.prog3.projectCycling.domain.Team;
import be.kdg.prog3.projectCycling.exceptions.InvalidFilterValueException;
import be.kdg.prog3.projectCycling.repository.springData.SpringDataTeamRepository;
import be.kdg.prog3.projectCycling.service.TeamService;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Profile("springData")
public class SpringDataTeamService implements TeamService {

    private final SpringDataTeamRepository springDataTeamRepository;

    public SpringDataTeamService(SpringDataTeamRepository springDataTeamRepository) {
        this.springDataTeamRepository = springDataTeamRepository;
    }

    @Override
    public List<Team> getAllTeams() {
        return springDataTeamRepository.findAll();
    }

    @Override
    public List<Team> getAllTeamWithRiders() {
        return springDataTeamRepository.findAll();
    }

    @Override
    public void addTeam(Team team) {
        springDataTeamRepository.save(team);
    }

    @Override
    public Team updateTeam(Team team) {
        return springDataTeamRepository.save(team);
    }

    @Override
    public Team findById(int id) {
        try {
            Optional<Team> optionalTeam = springDataTeamRepository.findById(id);

            if (optionalTeam.isPresent()) {
                return optionalTeam.get();
            } else {
                throw new InvalidFilterValueException("Team with this id doesn't exist");
            }
        } catch (EmptyResultDataAccessException e) {
            throw new InvalidFilterValueException("Team with this id doesn't exist");
        }
    }


    @Override
    public void deleteTeam(int id) {
        Optional<Team> delteam = springDataTeamRepository.findById(id);

        if (delteam.isPresent()) {
            Team team = delteam.get();

            for (Cyclist cyclist : team.getCyclists()) {
                cyclist.setTeamId(null);
                cyclist.setTeam(null);
            }

            springDataTeamRepository.delete(team);
        }
    }
}
