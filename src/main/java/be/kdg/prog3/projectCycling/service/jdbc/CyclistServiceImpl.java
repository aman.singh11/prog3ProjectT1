package be.kdg.prog3.projectCycling.service.jdbc;

import be.kdg.prog3.projectCycling.domain.Cyclist;
import be.kdg.prog3.projectCycling.exceptions.DataAccessException;
import be.kdg.prog3.projectCycling.exceptions.InvalidFilterValueException;
import be.kdg.prog3.projectCycling.repository.CyclistRepository;
import be.kdg.prog3.projectCycling.service.CyclistService;
import be.kdg.prog3.projectCycling.service.springData.SpringDataCyclistService;
import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Profile({"jdbc", "jpa"})
public class CyclistServiceImpl implements CyclistService {

    private final CyclistRepository cyclistRepository;

    private static final Logger logger = LoggerFactory.getLogger(SpringDataCyclistService.class);


    @Autowired
    public CyclistServiceImpl(CyclistRepository cyclistRepository) {
        this.cyclistRepository = cyclistRepository;
    }

    public List<Cyclist> getAllCyclists() {
        try {
            return cyclistRepository.findAll();
        } catch (DataAccessException ex) {
            throw new DataAccessException("Error occurred while fetching cyclists from the database.");
        }
    }

    @Override
    public List<Cyclist> getAllCyclistsWithTeamAndRaces() {
        return cyclistRepository.findAllWithTeamAndRace();
    }

    @Override
    public Cyclist getCyclistsById(int id) {
        try {
            Cyclist cyclist = cyclistRepository.findById(id);
            if (cyclist == null || cyclist.getId() == 0)
                throw new InvalidFilterValueException("ID cannot be found in the system!");
            return cyclist;
        } catch (NullPointerException e) {
            logger.error("An error occurred finding the id.", e);
            throw new InvalidFilterValueException("ID cannot be found in the system!");
        }
    }


    @Override
    @Transactional
    public Cyclist updateCyclist(Cyclist cyclist) {
        try {
            cyclistRepository.updateCyclist(cyclist);
        } catch (Exception e) {
            logger.error("An error occurred updating.", e);
        }
        return cyclist;
    }


    @Override
    public Cyclist addCyclist(Cyclist cyclist) {
        cyclistRepository.createCyclist(cyclist);
        return cyclist;
    }


    @Override
    public void deleteCyclist(int id) {
        cyclistRepository.deleteCyclist(id);
    }

    @Override
    public List<Cyclist> getYoungCyclist() {
        try {
            List<Cyclist> youngCyclists = cyclistRepository.getYoungCyclist();
            handleEmptyResult(youngCyclists, "No young cyclists found.");
            return youngCyclists;
        } catch (Exception e) {
            logger.error("An error occurred while retrieving young cyclists.", e);
            throw new InvalidFilterValueException("Error retrieving young cyclists.");
        }
    }

    @Override
    public List<Cyclist> getOldCyclist() {
        try {
            List<Cyclist> oldCyclists = cyclistRepository.getOldCyclist();
            handleEmptyResult(oldCyclists, "No old cyclists found.");
            return oldCyclists;
        } catch (Exception e) {
            logger.error("An error occurred while retrieving old cyclists.", e);
            throw new InvalidFilterValueException("Error retrieving old cyclists.");
        }
    }

    @Override
    public List<Cyclist> getCyclistWithMostWins() {
        try {
            List<Cyclist> cyclistsWithMostWins = cyclistRepository.getCyclistWithMostWins();
            handleEmptyResult(cyclistsWithMostWins, "No cyclists with most wins found.");
            return cyclistsWithMostWins;
        } catch (Exception e) {
            logger.error("An error occurred while retrieving cyclists with most wins.", e);
            throw new InvalidFilterValueException("Error retrieving cyclists with most wins.");
        }
    }

    private void handleEmptyResult(List<?> resultList, String message) {
        if (resultList.isEmpty()) {
            logger.warn(message);
            throw new InvalidFilterValueException(message);
        }
    }

}
