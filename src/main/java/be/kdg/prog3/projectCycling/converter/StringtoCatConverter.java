package be.kdg.prog3.projectCycling.converter;

import be.kdg.prog3.projectCycling.domain.Race;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class StringtoCatConverter implements Converter<String, Race.RaceCategory> {
    @Override
    public Race.RaceCategory convert(String source) {
        try {
            int categoryCode = Integer.parseInt(source);
            return switch (categoryCode) {
                case 1 -> Race.RaceCategory.GRAND_TOUR;
                case 2 -> Race.RaceCategory.CLASSICS;
                case 3 -> Race.RaceCategory.MONUMENTS;
                case 4 -> Race.RaceCategory.STAGES;
                default -> Race.RaceCategory.ONEDAY;
            };
        } catch (NumberFormatException e) {
            return Race.RaceCategory.DEFAULT;
        }
    }
}
