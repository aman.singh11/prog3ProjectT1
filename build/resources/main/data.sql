-- Insert data into TEAMS
INSERT INTO TEAMS (TEAMNAME, SPONSOR, COUNTRY, BIKEBRAND, FOUNDED)
VALUES ('Ineos Grenadiers', 'Ineos', 'United Kingdom', 'Pinarello', 2010),
       ('Alpecin–Deceuninck', 'Alpecin', 'Belgium', 'Canyon', 2008),
       ('UAE Team Emirates', 'Colnago', 'UAE', 'Colnago', 1999),
       ('Soudal Quick-Step', 'Quick-Step', 'Belgium', 'Specialized', 2003),
       ('Team Jumbo-Visma', 'Jumbo', 'Netherlands', 'Cervelo', 1984),
       ('BORA-hansgrohe', 'BORA', 'Germany', 'Specialized', 2010);


-- Insert data into CYCLISTS
INSERT INTO CYCLISTS (NAME, AGE, NATIONALITY, HEIGHT, TEAM_ID)
VALUES ('Mathieu van der Poel', 28, 'Dutch', 1.84, 2),
       ('Kaden Groves', 24, 'Australian', 1.76, 2),
       ('Tadej Pogačar', 25, 'Slovenian', 1.76, 3),
       ('Primoz Roglic', 33, 'Slovenian', 1.77, 5),
       ('Chris Froome', 36, 'British', 1.86, 1),
       ('Joshua Tarling', 19, 'British', 1.94, 1),
       ('Jasper Philipsen', 25, 'Belgian', 1.76, 2),
       ('Wout Van Aert', 29, 'Belgian', 1.9, 5),
       ('Filippo Ganna', 27, 'Italian', 1.93, 1),
       ('Remco Evenepoel', 23, 'Belgian', 1.71, 4),
       ('Egan Bernal', 24, 'Colombian', 1.75, 1),
       ('Peter Sagan', 31, 'Slovakian', 1.84, 6),
       ('Sepp Kuss', 29, 'American', 1.82, 5),
       ('Jonas Vingegaard', 26, 'Danish', 1.75, 5);

-- Insert data into RACES
INSERT INTO RACES (RACENAME, RACEDATE, LOCATION, CATEGORY, CYCLIST_ID)
VALUES ('Tour de France', '2023-07-01', 'France', 'GRAND_TOUR', 14),
       ('Giro d''Italia', '2023-05-08', 'Italy', 'GRAND_TOUR', 4),
       ('Vuelta a España', '2023-08-19', 'Spain', 'GRAND_TOUR', 13),
       ('Paris-Roubaix', '2023-04-09', 'France', 'MONUMENTS', 1),
       ('Amstel Gold Race', '2023-04-09', 'Netherlands', 'CLASSICS', 3),
       ('Tour of Flanders', '2023-04-09', 'Belgium', 'MONUMENTS', 3),
       ('Milan Sanremo', '2023-04-09', 'Italy', 'MONUMENTS', 1),
       ('Gent - Wevelgem', '2023-04-09', 'Belgium', 'CLASSICS', 8),
       ('Tour of Belgium', '2023-04-09', 'Belgium', 'STAGES', 1),
       ('UCI World Championship', '2023-04-09', 'Scotland', 'ONEDAY', 1);

-- Insert data into CYCLIST_RACE
INSERT INTO CYCLIST_RACE (ID_Cyclist, ID_Races)
VALUES (14, 1),
       (4, 2),
       (13, 3),
       (1, 4),
       (3, 5),
       (3, 6),
       (1, 7),
       (8, 8),
       (1, 9),
       (1, 10);





