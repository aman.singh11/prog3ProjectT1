DROP TABLE IF EXISTS CYCLIST_RACE;
DROP TABLE IF EXISTS RACES;
DROP TABLE IF EXISTS CYCLISTS;
DROP TABLE IF EXISTS TEAMS;


-- Table definition for TEAMS
CREATE TABLE TEAMS
(
    ID        INTEGER AUTO_INCREMENT,
    TEAMNAME  VARCHAR(30),
    SPONSOR   VARCHAR(30) NOT NULL,
    COUNTRY   VARCHAR(30) NOT NULL,
    BIKEBRAND VARCHAR(30) NOT NULL,
    FOUNDED   INTEGER     NOT NULL,
    CONSTRAINT TEAMS_pk PRIMARY KEY (ID)
);

-- Table definition for CYCLISTS
CREATE TABLE CYCLISTS
(
    ID          INT AUTO_INCREMENT,
    NAME        VARCHAR(30),
    AGE         INTEGER     NOT NULL,
    NATIONALITY VARCHAR(30) NOT NULL,
    HEIGHT      DOUBLE,
    TEAM_ID     INTEGER,
    CONSTRAINT Cyclist_pk PRIMARY KEY (ID),
    CONSTRAINT FK_CYCLISTS_TEAM_ID
        FOREIGN KEY (TEAM_ID) REFERENCES TEAMS (ID)
);


-- Table definition for RACES
CREATE TABLE RACES
(
    ID         INTEGER AUTO_INCREMENT,
    RACENAME   VARCHAR(30),
    RACEDATE   DATE        NOT NULL,
    LOCATION   VARCHAR(30) NOT NULL,
    CATEGORY   VARCHAR(30) NOT NULL,
    CYCLIST_ID INTEGER,
    CONSTRAINT Race_pk PRIMARY KEY (ID),
    CONSTRAINT FK_CYCLISTS_RACES
        FOREIGN KEY (CYCLIST_ID) REFERENCES CYCLISTS (ID)
);

CREATE TABLE CYCLIST_RACE
(
    ID_Cyclist INTEGER,
    ID_Races   INTEGER,
    foreign key (ID_Cyclist) references CYCLISTS (ID),
    foreign key (ID_Races) references RACES (ID),
    constraint "relationship_pk" primary key (ID_Races, ID_Cyclist)
);



